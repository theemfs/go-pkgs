package ctx

import (
	"context"
)

type key struct{}

var ctxKey key

func Set(ctx context.Context, ip string) context.Context {
	return context.WithValue(ctx, ctxKey, ip)
}

func Get(ctx context.Context) string {
	if ip, ok := ctx.Value(ctxKey).(string); ok {
		return ip
	}
	return ""
}

func IsSet(ctx context.Context) bool {
	return Get(ctx) != ""
}
