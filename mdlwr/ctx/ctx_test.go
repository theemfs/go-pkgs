package ctx

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetGet(t *testing.T) {
	ctx := context.Background()
	assert.False(t, IsSet(ctx))
	ip := "8.8.8.8"
	newCtx := Set(ctx, ip)
	assert.Equal(t, ip, Get(newCtx))
	assert.True(t, IsSet(newCtx))
}
