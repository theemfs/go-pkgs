package mdlwr

import (
	"go-pkgs/mdlwr/ctx"
	"net"
	"net/http"
	"strings"

	"github.com/rs/zerolog"
)

var (
	trueClientIP  = http.CanonicalHeaderKey("True-Client-IP")
	xForwardedFor = http.CanonicalHeaderKey("X-Forwarded-For")
	xRealIP       = http.CanonicalHeaderKey("X-Real-IP")
)

func RealIP(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		if ipAddr := realIP(r); ipAddr != "" {
			r.RemoteAddr = ipAddr
			r = r.WithContext(ctx.Set(r.Context(), ipAddr))
		}
		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func realIP(r *http.Request) string {
	var ip, source string

	if tcip := r.Header.Get(trueClientIP); tcip != "" {
		ip = tcip
		source = trueClientIP
	} else if xrip := r.Header.Get(xRealIP); xrip != "" {
		ip = xrip
		source = xRealIP
	} else if xff := r.Header.Get(xForwardedFor); xff != "" {
		i := strings.Index(xff, ",")
		if i == -1 {
			i = len(xff)
		}
		ip = xff[:i]
		source = xForwardedFor
	}
	if ip == "" || net.ParseIP(ip) == nil {
		return ""
	}

	zerolog.Ctx(r.Context()).Debug().Str("source", source).Str("IP from headers", ip).Send()
	return ip
}
