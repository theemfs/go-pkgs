package mdlwr

import (
	"errors"
	"net/http"
	"runtime/debug"

	"github.com/go-chi/render"
	"github.com/rs/zerolog"
)

func Recover(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if info := recover(); info != nil {
				logger := zerolog.Ctx(r.Context())
				stack := debug.Stack()
				err := errors.New("panic on request")
				logger.Error().Err(err).Interface("recover_info", info).Bytes("debug_stack", stack).Send()
				render.Respond(w, r, err)
			}
		}()
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
