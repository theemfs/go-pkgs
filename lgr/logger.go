package lgr

import (
	"os"

	"github.com/rs/zerolog"
)

func New() zerolog.Logger {
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()

	// zerolog.CallerMarshalFunc = func(file string, line int) string {
	// 	var n int
	// 	for i := len(file) - 1; i >= 0; i-- {
	// 		if file[i] == '/' {
	// 			n++
	// 			if n == 2 {
	// 				file = file[i+1:]
	// 				break
	// 			}
	// 		}
	// 	}
	// 	return file + ":" + strconv.Itoa(line)
	// }

	const withCaller = true
	if withCaller {
		logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Caller().Logger()
	}

	return logger
}
