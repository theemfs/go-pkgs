package lgr

import (
	"encoding/json"
	"fmt"
	"strings"
)

const (
	delimiter = "==========================="
)

// Prettify makes nice human-readable string
func Prettify(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")

	return string(s)
}

// PP is a Prettified Print
func PP(i ...interface{}) {
	fmt.Println(delimiter)

	for _, j := range i {
		fmt.Println(Prettify(j))
	}

	fmt.Println(delimiter)
}

// GetPP returns Prettified Print
func GetPP(i ...interface{}) string {
	r := []string{}
	for _, j := range i {
		r = append(r, Prettify(j))
	}

	return strings.Join(r[:], "")
}

// GetJSON returns JSON
func GetJSON(i interface{}) string {
	bytes, _ := json.Marshal(i)

	return string(bytes)
}
