package sql

// drivers
const (
	DriverClickhouse = "clickhouse"
	DriverPG         = "postgres"
	DriverMSSQL      = "mssql"
)
