package sql

import (
	"context"
	"database/sql"

	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
)

func New(_ context.Context, cfg Config) (*sqlx.DB, error) {
	db, err := sql.Open(cfg.Driver, cfg.ConnectionString)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}
	dbx := sqlx.NewDb(db, cfg.Driver)

	dbx.SetMaxOpenConns(cfg.MaxOpenConnections)
	dbx.SetMaxIdleConns(cfg.MaxIdleConnections)
	dbx.SetConnMaxLifetime(cfg.MaxLifetimeOfConnections)

	return dbx, nil
}
