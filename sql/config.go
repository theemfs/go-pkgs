package sql

import (
	"time"
)

type Config struct {
	ConnectionString         string        `mapstructure:"connection_string" validate:"required"`
	Driver                   string        `mapstructure:"driver" validate:"required"`
	MaxOpenConnections       int           `mapstructure:"max_open_connections" validate:"required"`
	MaxIdleConnections       int           `mapstructure:"max_idle_connections" validate:"required"`
	MaxLifetimeOfConnections time.Duration `mapstructure:"max_lifetime_of_connections" validate:"required"`
}
