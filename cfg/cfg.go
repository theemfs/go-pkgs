package cfg

type App struct {
	Name string `mapstructure:"name" validate:"required"`
	Port int    `mapstructure:"port" validate:"required"`
}
