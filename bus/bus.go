package bus

import (
	"encoding/json"
	"sync"
)

type Interface interface {
	Sub(subscriber string, topic string, fn func(json.RawMessage))
	Pub(topic string, data json.RawMessage)
}

type Bus struct {
	lock      sync.Mutex
	callbacks map[string][]func(json.RawMessage)
}

func New() *Bus {
	m := make(map[string][]func(json.RawMessage))
	return &Bus{
		callbacks: m,
	}
}

func (s *Bus) Sub(subscriber string, topic string, fn func(json.RawMessage)) {
	s.lock.Lock()
	defer s.lock.Unlock()

	print("new subscriber " + subscriber + " to topic " + topic + "\n")

	s.callbacks[topic] = append(s.callbacks[topic], fn)
}

func (s *Bus) Pub(topic string, data json.RawMessage) {
	s.lock.Lock()
	defer s.lock.Unlock()

	callbacks, ok := s.callbacks[topic]
	if ok {
		for _, callback := range callbacks {
			print("run callback\n")
			callback(data)
		}
	}
}
