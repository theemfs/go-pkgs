package consumer

type Interface interface {
	Read() (*Message, error)
	Close() error
	// Sub(topic string, fn func(json.RawMessage))
}
