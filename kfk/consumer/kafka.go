package consumer

import (
	"context"

	"github.com/segmentio/kafka-go"
)

type KafkaReader struct {
	ctx         context.Context
	kafkaReader *kafka.Reader
}

type Config struct {
	BrokerList []string `mapstructure:"broker_list" validate:"required"`
	GroupID    string   `mapstructure:"group_id" validate:"required"`
	Topic      string   `mapstructure:"topic" validate:"required"`
	Topics     []string `mapstructure:"topics"`
}

func NewKafkaReader(ctx context.Context, cfg Config) *KafkaReader {
	var topics []string
	topics = append(topics, cfg.Topics...)

	return &KafkaReader{
		ctx: ctx,
		kafkaReader: kafka.NewReader(kafka.ReaderConfig{
			Brokers:     cfg.BrokerList,
			GroupID:     cfg.GroupID,
			Topic:       cfg.Topic,
			GroupTopics: topics,
		}),
	}
}

type Message struct {
	Key   []byte
	Value []byte
}

func (kr *KafkaReader) Read() (*Message, error) {
	msg, err := kr.kafkaReader.ReadMessage(kr.ctx)
	if err != nil {
		return nil, err
	}

	return &Message{
		Key:   msg.Key,
		Value: msg.Value,
	}, nil
}

func (kr *KafkaReader) Close() error {
	return kr.kafkaReader.Close()
}
