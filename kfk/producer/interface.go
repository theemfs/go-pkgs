package producer

type Interface interface {
	Write([]Message) error
	Close() error
}
