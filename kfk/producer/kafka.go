package producer

import (
	"context"

	"github.com/segmentio/kafka-go"
)

type Config struct {
	BrokerList []string `mapstructure:"broker_list" validate:"required"`
	GroupID    string   `mapstructure:"group_id" validate:"required"`
	Topic      string   `mapstructure:"topic" validate:"required"`
	Topics     []string `mapstructure:"topics"`
	// BatchSize    int      `mapstructure:"topic" validate:"required"`
	// Async        bool     `mapstructure:"async" validate:"required"`
}

type KafkaWriter struct {
	ctx         context.Context
	kafkaWriter *kafka.Writer
}

func NewKafkaWriter(ctx context.Context, cfg Config) *KafkaWriter {
	return &KafkaWriter{
		ctx: ctx,
		kafkaWriter: kafka.NewWriter(
			kafka.WriterConfig{
				Brokers:   cfg.BrokerList,
				Topic:     cfg.Topic,
				Async:     true,
				BatchSize: 1,
			}),
	}
}

type Message struct {
	Key   []byte
	Value []byte
}

func (kw *KafkaWriter) Write(msgs []Message) error {
	var messages []kafka.Message
	for _, msg := range msgs {
		messages = append(messages, kafka.Message{
			Key:   msg.Key,
			Value: msg.Value,
		})
	}

	return kw.kafkaWriter.WriteMessages(kw.ctx, messages...)
}

func (kw *KafkaWriter) Close() error {
	return kw.kafkaWriter.Close()
}
