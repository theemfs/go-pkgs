package kfk

type Config struct {
	BrokerList []string `mapstructure:"broker_list" validate:"required"`
	GroupID    string   `mapstructure:"group_id"`
	// BatchTimeout    time.Duration `mapstructure:"batch_timeout"`
	// ReadTimeout     time.Duration `mapstructure:"read_timeout"`
	// WriteTimeout    time.Duration `mapstructure:"write_timeout"`
	// MaxWait         time.Duration `mapstructure:"max_wait"`
	// CommitInterval  time.Duration `mapstructure:"commit_interval"`
	// ReadBackOffMax  time.Duration `mapstructure:"read_back_off_max"`
	// ReadBackOffMin  time.Duration `mapstructure:"read_back_off_min"`
	// ReadMaxBytes    int           `mapstructure:"read_max_bytes"`
	// WriteBatchBytes int           `mapstructure:"write_batch_bytes"`
	// Limit           uint          `mapstructure:"limit"`
}
